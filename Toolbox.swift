//
//  Toolbox.swift
//  gHash
//
//  Created by Brendan White on 5/1/21.
//

import Foundation

extension Double {
    var km: Double { return self }
    var miles: Double { return self / 0.621371 }
}


extension Date {
    var yesterday: Date { return self.dayBefore }
    var tomorrow:  Date { return self.dayAfter }
    
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
    func days (to otherDate:Date) -> Int {
        Calendar.current.dateComponents(
            [.day], from: noon, to: otherDate.noon
        ).day!
    }
}
