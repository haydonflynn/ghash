//
//  Bulletin.swift
//  gHash
//
//  Created by Brendan White on 5/1/21.
//

import Foundation
import SwiftUI
import MapKit



struct Bulletin {
    
    var bulletinDate:Date = Date()
    var bulletinLocation:CLLocationCoordinate2D? = nil
    
    var mainHashpoint: Hashpoint? = nil
    var otherHashPoints: [Hashpoint] = []
    
    
    //@Binding var settings:Settings
    //@Binding var currentLocation: CLLocationCoordinate2D?
    
    var graticuleWidgetText: String {
        get {
            if 0//settings.selectedUnits
                == Settings.KM_INDEX {
                return "---, ---"
            } else {
                return "-00, 000"
            }
            
        } // end getter
    } // end graticuleWidgetText
    
    
    var distanceWidgetText: String {
        get {
            if 0//settings.selectedUnits
                == Settings.KM_INDEX {
                return "--.- km"
            } else {
                return "--.- miles"
            }
            
        } // end getter
    } // end distanceWidgetText
    
    var dateWidgetText: String {
        get {
            let dateWidgetDateFormatter = DateFormatter()
            dateWidgetDateFormatter.dateFormat = "d MMM" // eg "25 Dec" or "1 Jan"
            
            return dateWidgetDateFormatter.string(
                from: bulletinDate
            ).capitalized // eg "25 Dec" or "1 Jan"
        }
    } // end dateWidget
    
    var heroText: String {
        
        let heroDateFormatter = DateFormatter()
        heroDateFormatter.dateFormat = "EEEE" // eg "Monday" or "Wednesday"
        let heroNumberFormatter = NumberFormatter()
        heroNumberFormatter.numberStyle = .spellOut // eg "Three" or "Four"
        
        // How many days from today to the bulletin date?
        let daysOffset: Int = Date().days(to: bulletinDate)
        
        // Now return the hero text for the bulletin date
        if daysOffset == 0 {
            return "Today"
        } else if daysOffset == 1 {
            return "Tomorrow"
        } else if daysOffset > 1 && daysOffset <= 6 {
            return heroDateFormatter.string(
                from: bulletinDate
            ).capitalized // eg "Monday" or "Wednesday"
        } else if daysOffset > 6 {
            return "a long way in the future" // this should never happen
        } else if daysOffset == -1 {
            return "Yesterday"
        } else if daysOffset <= -2 && daysOffset >= -20 {
            return "\(heroNumberFormatter.string(from: NSNumber(value: -1 * daysOffset))!.capitalized) Days Ago" // eg "Sixteen days ago"
        } else if daysOffset < -20 {
            return "\(-1 * daysOffset) Days Ago" // eg "46 days ago"
            
        } else {
            return "hero text not set" // eg "46 days ago"
        }
        
    } // end heroText
    
    
    
    
    
    
    
    
} // end struct Bulletin
