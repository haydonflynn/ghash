//
//  ContentView.swift
//  gHash
//
//  Created by Brendan White on 28/12/20.
//

import SwiftUI
import MapKit

struct ContentView: View {
    
    @State private var settings: Settings = Settings()
    
    @State var locationManager = CLLocationManager()
    
    @State var currentLocation: CLLocationCoordinate2D? = nil
    
    @State var theBulletin = Bulletin()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        print("Got new locations!!")
            print(locations)
        if  let coordinate = locationManager.location?.coordinate {
            region.center = coordinate
            currentLocation = coordinate
            
        }

    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            print("Location notDetermined ")
            
        case .restricted:
            print("Location restricted ")
            
        case .denied:
            print("Location denied ")
            
        case .authorizedAlways, .authorizedWhenInUse:
            print("Location authorizedWhenInUse ")
            manager.startUpdatingLocation()
            
        default:
            print("Location NOT authorised ")

        } // end switch
        
    } // end func locationManager
    


func setupManager() {
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.requestWhenInUseAuthorization()
    //locationManager.requestAlwaysAuthorization()
}

@State private var showingSettings = false

fileprivate func InfoBar() -> some View {
    return HStack{
        
        VStack{
            Text("Date")
            Text(theBulletin.dateWidgetText).font(.subheadline)
        }
        Spacer()
        VStack{
            Text("Distance")
            Text(theBulletin.distanceWidgetText).font(.title)
        }
        Spacer()
        VStack{
            Text("Grat")
            Text(theBulletin.graticuleWidgetText).font(.subheadline)
        }
        
    }.padding()  // end HStack
} // end InfoBar

fileprivate func mapURL() -> String {
    //print("selectedMapApp: \(settings.selectedMapApp)")
    if (settings.selectedMapApp  == Settings.APPLE_MAPS_INDEX) {
        return "http://maps.apple.com?daddr=-35.013056,138.655833"
    } else {
        return "https://www.google.com/maps/dir/?api=1&destination=-35.013056%2C138.655833"
    }
} // end func

struct HashPoint: Identifiable {
    var id = UUID()
    let name: String
    let latitude: Double
    let longitude: Double
    let tint: Color;
    
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}


let hashPoints = [
    HashPoint(name: "Today", latitude: -35.013056, longitude: 138.655833, tint:.accentColor),
    HashPoint(name: "Tomorrow", latitude: -34.984, longitude: 138.651, tint:.secondary),
    HashPoint(name: "Monday", latitude: -35.01317, longitude: 138.69082, tint:.secondary)
]


@State private var region = MKCoordinateRegion(
    center: CLLocationCoordinate2D(latitude: -35.013056, longitude: 138.655833),
    span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1)
)

@State private var userTrackingMode: MapUserTrackingMode = .follow

fileprivate func theMap() -> Map<_DefaultAnnotatedMapContent<[ContentView.HashPoint]>> {
    return Map(coordinateRegion: $region,
               interactionModes: MapInteractionModes.all,
               showsUserLocation: true,
               userTrackingMode: $userTrackingMode,
               annotationItems: hashPoints
               
    ) { place in
        MapMarker(coordinate: place.coordinate, tint:place.tint)
    }
}

fileprivate func needLocation() -> some View {
    return
        
        VStack{
            Spacer()
            Text("GPS Location").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/).padding()
            Text("We need your GPS location to find your nearest hashpoint.").padding()
            Button(
                action: {
                    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    self.locationManager.requestAlwaysAuthorization()
                    //self.locationManager.requestWhenInUseAuthorization()
                    self.locationManager.startUpdatingLocation()
                    currentLocation = locationManager.location?.coordinate
                    
                },
                label: {Text("Get GPS Location")}
            ).buttonStyle(BorderlessButtonStyle()).padding()
            Spacer()
            
        }.padding()
}

var body: some View {
    
    NavigationView {
        Group{
            
            if  (currentLocation) != nil {
                theMap()
            } else {
                ZStack{
                    theMap().blur(radius: 10.0)
                    needLocation()
                }
                
            }
            
            HStack{InfoBar()}
        }
        
        .sheet(isPresented: $showingSettings) {
            SettingsView(settings: $settings)
        }
        
        
        .navigationBarTitle(theBulletin.heroText, displayMode: .inline)
        .toolbar {
            
            ToolbarItem(placement: .navigationBarLeading) {
                Button(
                    action: {theBulletin = Bulletin(bulletinDate: theBulletin.bulletinDate.yesterday)},
                    label: {Image(systemName: "chevron.left")}).disabled(false)
            }
            
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(
                    action: {theBulletin = Bulletin(bulletinDate: theBulletin.bulletinDate.tomorrow)},
                    label: {Image(systemName: "chevron.right")}).disabled(false)
            }
            
            ToolbarItemGroup(placement: .bottomBar) {
                
                Button(
                    action: {self.showingSettings.toggle()},
                    label: {Image(systemName: "gearshape")}
                )
                
                Spacer()
                
                Button(
                    action: {
                        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                        self.locationManager.requestAlwaysAuthorization()
                        //self.locationManager.requestWhenInUseAuthorization()
                        self.locationManager.startUpdatingLocation()
                        
                    },
                    label: {Image(systemName: "questionmark.circle")}
                )
                
                Spacer()
                
                Button(
                    action: {
                        // re-centre map on current location
                        
                        
                        if  let coordinate = locationManager.location?.coordinate {
                            region.center = coordinate
                        }
                        
                        
                        print()
                        print()
                        print()
                        print("map region")
                        print(region.self)
                        print(locationManager.location?.coordinate ?? "no coordinate")
                        print()
                        print()
                        
                        
                    },
                    label: {Image(systemName: "location")}
                )
                
                Spacer()
                
                Link(destination: URL(string: "http://geo.crox.net/poster/2018-04-13_-34_138")!) {
                    Image(systemName: "doc.richtext")
                }
                
                Spacer()
                
                Link(destination: URL(string: mapURL())!) {
                    Image(systemName: "map")
                }
                
            } // end bottomBar
            
        } // end .toolbar
        
    } // end NavigationView
} // end body
} // end struct

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
            ContentView()
        }
    }
}
