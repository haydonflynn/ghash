//
//  Location.swift
//  gHash
//
//  Created by Brendan White on 3/1/21.
//

import Foundation
import MapKit
import SwiftUI


struct Location {
    
    @Binding var settings:Settings
    @Binding var locationManager:CLLocationManager
    
    func getLocation() -> CLLocationCoordinate2D? {
        
        return locationManager.location?.coordinate
        
    } // end getLocation()
    
    
} // end struct

