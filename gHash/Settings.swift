//
//  Settings.swift
//  gHash
//
//  Created by Brendan White on 30/12/20.
//

import Foundation

struct Settings {
    //static let settings = Settings() // make it a singleton
    
    public let daysOfHistory: Int = 7 // can change this to a variable, save it to UserDefaults, and add it to settings view if we want to
    
 
    
    
    
    
    public var selectedMapApp: Int = UserDefaults.standard.integer(forKey: "selectedMapApp") // defaults to zero if not yet set
    {
        didSet {
            UserDefaults.standard.set(selectedMapApp, forKey: "selectedMapApp")
        }
    }
    
    public static let mapAppOptions = ["Apple Maps", "Google Maps"]
    public static let APPLE_MAPS_INDEX: Int = 0
    public static let GOOGLE_MAPS_INDEX: Int = 1
    
    public var selectedUnits: Int = UserDefaults.standard.integer(forKey: "selectedUnits") // defaults to zero if not yet set
    {
        didSet {
            UserDefaults.standard.set(selectedUnits, forKey: "selectedUnits")
        }
    }
    public static let unitsOptions = ["Kilometres", "Miles"]
    public static let KM_INDEX: Int = 0
    public static let MILES_INDEX: Int = 1
    
    
    public var notificationOn : Bool = UserDefaults.standard.bool(forKey: "notificationOn") // defaults to false if not yet set
    {
        didSet {
            UserDefaults.standard.set(notificationOn, forKey: "notificationOn")
        }
    }
    public var notificationTime : Date
    {
        didSet {
            print("Attempting to write notificationTime back out to UserDefaults")
            // pull out hour and minute, just save those
            let components = Calendar.current.dateComponents([.hour, .minute], from: notificationTime)
            UserDefaults.standard.set(components.hour ?? 8, forKey: "notificationTimeHour")
            UserDefaults.standard.set(components.minute ?? 0, forKey: "notificationTimeMinute")
            print("...wrote out \(components.hour ?? -1) h \(components.minute ?? -1) m")
        }
    }
    public var maxDist: Int = UserDefaults.standard.integer(forKey: "maxDist") // defaults to zero if not yet set - will be fixed in init()
    {
        didSet {
            UserDefaults.standard.set(maxDist, forKey: "maxDist")
        }
    }
    
    
    init() { // make this private if you want it to be a singleton
        
        print("Attempting to get settings from UserDefaults")
        
        // getting the hour and minute, and converting them to a Date, is non-trivial
        var components = DateComponents()
        
        components.hour = UserDefaults.standard.integer(forKey: "notificationTimeHour") // defaults to zero if not yet set
        print("Hour is \(components.hour ?? -1)")
        components.minute = UserDefaults.standard.integer(forKey: "notificationTimeMinute") // defaults to zero if not yet set
        print("Minute is \(components.minute ?? -1)")
        
        if (components.hour == 0 && components.minute == 0) {
            print("Hour & Minute both zero - so reset to 8am")
            components.hour = 8
        } else {
            print("Hour and/or minute are non-zero - no need to reset them")
        }
        notificationTime = Calendar.current.date(from: components)! // if we can't build the date from components, just die
        
        if maxDist == 0 { maxDist = 10 } // if it defaulted to zero, reset it to ten
        
    } // end init()
    
    
} // end struct
