//
//  SettingsView.swift
//  gHash
//
//  Created by Brendan White on 29/12/20.
//

import SwiftUI

struct SettingsView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @Binding var settings:Settings

    
    var body: some View {
        NavigationView {
            
            Form {
                
                
                Section(header: Text("Measure Distance in")) {
                    Picker("Units", selection: $settings.selectedUnits) {
                        ForEach(0..<Settings.unitsOptions.count) {
                            Text(Settings.unitsOptions[$0])
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                } // end section
                
                Section(header: Text("Map Icon takes me to")) {
                    Picker("Map Icon App", selection: $settings.selectedMapApp) {
                        ForEach(0..<Settings.mapAppOptions.count) {
                            Text(Settings.mapAppOptions[$0])
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                } // end section
                
                Section(header: Text("Notify Me")) {
                    Toggle("Notify Me of Today's Location", isOn: $settings.notificationOn)
                    
                    if settings.notificationOn {
                        DatePicker("...at this time", selection: $settings.notificationTime,
                                   displayedComponents: .hourAndMinute)
                        Stepper( value: $settings.maxDist, in: 1...100) {
                            Text("...if it's within")
                                + Text(" \(settings.maxDist) ")
                                .fontWeight(.bold)
                                .font(.title)
                                + Text(
                                    (settings.selectedUnits==Settings.KM_INDEX)
                                        ? "km" : "miles"
                                )
                        } // end Stepper
                    } // end if settings.notificationOn
                    
                } // end section
                
            } // end form
            
            
            
            .navigationBarTitle(Text("Settings"), displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
                print("Dismissing settings view...")
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark").imageScale(.large)
            })
            
        } // end NavigationView
        
    } // end View
} // end struct

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            SettingsView(settings: .init(get: { () -> Settings in
                return Settings()
            }, set: { (Settings, Transaction) in
                
            }))
                .navigationBarTitle("Preview Navbar", displayMode: .inline)
        }
    }
}
