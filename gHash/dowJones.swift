//
//  dowJones.swift
//  gHash
//
//  Created by Brendan White on 1/1/21.
//

import SwiftUI
import Foundation

enum DowJonesError: Error {
    case notYetOpeningTime
    case noInternetConnection
    case couldntConnectToWebService
    case noOpeningValue
    case cantCheckAgainYet
    case retrievingNow
    case testError
}

struct DowJones {
    
    @Binding var settings:Settings
    
    private static let iso8601DateFormatter = DateFormatter()
    
    private static var whenWeCanCheckTheWebServiceAgain:Date = Date()
    //private static var whenWeCanCheckTheWebServiceAgain:Date = Calendar.current.date(byAdding: .second, value: -1, to: Date())!

    static func iso8601 (date:Date) -> String {
        
        iso8601DateFormatter.dateFormat = "yyyy-MM-dd"
        return iso8601DateFormatter.string(from: date)
        
    } // end func iso8601
    
    private static func dowJonesOpenTimeFor(date:Date) -> Date {
        
        print ("Getting DJ Open Time for \(date)")
        
        // first, extract the day, month, and year from the input date
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date)
        print ("Components are year \(components.year!), month \(components.month!), day \(components.day!)")
        
        // DJ opens at 9.30am (New York time)
        // but we'll check at 9.35 to give it a few minutes to update
        var djOpenComponents = DateComponents()
        djOpenComponents.year = components.year!
        djOpenComponents.month = components.month!
        djOpenComponents.day = components.day!
        djOpenComponents.hour = 9
        djOpenComponents.minute = 35
        djOpenComponents.timeZone = TimeZone(identifier: "America/New_York")
        
        // if this doesn't give an actual time we've got bigger problems .. just die
        let djOpenTime = Calendar.current.date(from:djOpenComponents)!
        print ("DJ Open time is \(djOpenTime)")
        
        return djOpenTime
        
    } // end dowJonesOpenTimeFor
    
    static func getDowJones(date: Date) throws -> Double {
        var dowJones:Double = 0.0
        let dateString:String = iso8601(date: date)
        print ("Date String for \(date) is \(dateString)")
        
        // First, check if we've already got the DJ for that date
        dowJones = UserDefaults.standard.double(forKey: dateString)
        if (dowJones > 0.0) {
            print ("Already got \(dowJones) for \(date)")
            return dowJones
        }
        
        // Then, check if it's after the open time for that date
        let dowJonesOpenTime:Date = dowJonesOpenTimeFor(date:date)
        if (Date() < dowJonesOpenTime) {
            print ("Opening time for \(date) is \(dowJonesOpenTime) which is in the future")
            throw DowJonesError.notYetOpeningTime
        }
        
        // If we've got this far, the DJ should be available but we don't have it yet.
        
        // Before we try to retreive it, let's check if we tried only a moment ago.
        let now = Date()
        if (now < whenWeCanCheckTheWebServiceAgain) {
            
            let df = DateFormatter()
            df.dateFormat = "y-MM-dd H:m:ss.SSSS"

            
            
            print ("It's currently \(df.string(from: now)), "
                   + "can't check again till \(df.string(from: whenWeCanCheckTheWebServiceAgain)) "
                    + "distance is \(now.distance(to: whenWeCanCheckTheWebServiceAgain))")
            throw DowJonesError.cantCheckAgainYet
        }
        
        // OK, let's trigger the job to retrieve it now
        requestDowJonesFor(dateString:dateString)
        throw DowJonesError.retrievingNow
        
    } // end getDowJones
    
    
    
    private static func handleClientError(dateString:String, error:Error) {
        // TODO: Write body
        // can't throw anything here
        print("")
        print("")
        print("got client error for date string '\(dateString)'")
        print("")
        print("")
    } // end func handleClientError
    
    private static func handleServerError(dateString:String,response:URLResponse?=nil) {
        // TODO: Write body
        // can't throw anything here
        print("")
        print("")
        let status = (response as! HTTPURLResponse).statusCode
        print("got server status \(status) for date string '\(dateString)'")
        print("")
        print("")
    } // end func handleClientError
    
    private static func handleReceivedHttpResponse(dateString:String, string:String, url: URL, mimeType:String) {
        // TODO: Write body
        // can't throw anything here
        print("")
        print("")
        print("got response '\(string)' of type '\(mimeType)' from url '\(url.absoluteString)' ")
        // got response '30415.09' of type 'text/plain' from url 'http://geo.crox.net/djia/2020-12-30'
        print("")
        print("")
    } // end func handleClientError
    
    
    
    
    
    private static func requestDowJonesFor(dateString:String) {
        // largely stolen from https://developer.apple.com/documentation/foundation/url_loading_system/fetching_website_data_into_memory
        
        var url:URL
        if (ProcessInfo.processInfo.environment["test_webserver"] == "foobarx") {
            //url = URL(string: "http://foobar.test/djia/"+dateString)!
            url = URL(string: "http://www.msftncsi.com/ncsi.txt")!
        } else {
             url = URL(string: "http://geo.crox.net/djia/"+dateString)!
        }
        print("Attempting to get DJIA from "+url.absoluteString)
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                self.handleClientError(dateString:dateString, error:error)
                return
            }
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                self.handleServerError(dateString:dateString, response:response)
                return
            }
            if let mimeType = httpResponse.mimeType, //mimeType == "text/html",
               let data = data,
               let string = String(data: data, encoding: .utf8) {
                DispatchQueue.main.async {
                    self.handleReceivedHttpResponse(dateString:dateString, string:string, url: url, mimeType:mimeType)
                }
            }
        }
        task.resume()
        
    } // end func requestDowJonesFor
    
    
    
    static func testError() throws  {
        throw DowJonesError.testError
    } // end testError
    
} // end struct
