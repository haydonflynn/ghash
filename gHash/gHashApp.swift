//
//  gHashApp.swift
//  gHash
//
//  Created by Brendan White on 28/12/20.
//

import SwiftUI

@main
struct gHashApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
